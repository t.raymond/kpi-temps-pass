USE [Projet-N2-KPI]
GO

INSERT INTO [dbo].[Employee]
           ([Name]
           ,[First_name]
           ,[Apprentice])
     VALUES
		   ('Martinez','Allan',0),
		   ('Breton','Julien',0),
		   ('Cossedu','David',0),
		   ('Patault','Alban',1),
		   ('Marchese','Reuben',0),
		   ('Bertrand','Guillaume',0),
		   ('Douchet','Loic',0),
		   ('Renoult','Manon',1),
		   ('Permanne','Johann',0),
		   ('Bouchevreau','Théo',1)


GO

INSERT INTO [dbo].[Customer]
           ([Wording])
     VALUES
           ('AppliDev'''),
		   ('LSDH'),
		   ('Petit Potager'),
		   ('L''antre d''eux'),
		   ('Seqens'),
		   ('Tipiak'),
		   ('Groupe Avril')
GO

INSERT INTO [dbo].[Assignment]
           ([Wording]
           ,[Display_order]
           ,[Billable])
     VALUES
           ('Non facturable',1,0),
		   ('Contrat de maintenance',2,1),
		   ('Projet interne',3,0),
		   ('Facturable',4,1)
GO

INSERT INTO [dbo].[Project]
           ([Wording]
           ,[Estimated_time]
           ,[Selection_time_spent]
           ,[Display_kpi]
           ,[FK_ID_Customer])
     VALUES
           ('Applidev',0.0,1,1,1),
		   ('Site MyMasterPlan',0.0,1,1,1),
		   ('FiF',0.0,1,1,2),
		   ('Petit Potager',0.0,1,1,3),
		   ('L''antre d''eux',0.0,1,1,4),
		   ('Seqens',0.0,1,1,5),
		   ('Tipiak TTP',0.0,1,1,6),
		   ('Sonate',0.0,1,1,7)
GO

INSERT INTO [dbo].[Task]
           ([Wording]
           ,[FK_ID_Project]
           ,[FK_ID_Assignment])
     VALUES
           ('Réunion hebdomadaire',1,1),
		   ('Développement',1,1),
		   ('Développement',4,4),
		   ('Développement',2,1),
		   ('Développement',3,4),
		   ('Développement',5,4),
		   ('Développement',6,4),
		   ('Développement',7,4),
		   ('Développement',8,4),
		   ('Réunion',1,1),
		   ('Correction de bugs',3,2),
		   ('Correction de bugs',5,2),
		   ('Correction de bugs',8,2),
		   ('Gestion de projet',1,1),
		   ('Gestion de projet',2,1),
		   ('Tests',6,4),
		   ('',1,1),
		   ('',7,4)

GO



