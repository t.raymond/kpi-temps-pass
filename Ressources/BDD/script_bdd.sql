USE [Projet-N2-KPI]
GO
/****** Object:  Table [dbo].[Task]    Script Date: 08/01/2020 11:07:42 ******/
DROP TABLE [dbo].[Task]
/****** Object:  Table [dbo].[Record]    Script Date: 08/01/2020 09:45:08 ******/
DROP TABLE [dbo].[Record]
/****** Object:  Table [dbo].[Project]    Script Date: 08/01/2020 09:47:50 ******/
DROP TABLE [dbo].[Project]
/****** Object:  Table [dbo].[Employee]    Script Date: 08/01/2020 09:43:30 ******/
DROP TABLE [dbo].[Employee]
/****** Object:  Table [dbo].[Customer]    Script Date: 08/01/2020 09:38:35 ******/
DROP TABLE [dbo].[Customer]
/****** Object:  Table [dbo].[Assignment]    Script Date: 08/01/2020 09:39:56 ******/
DROP TABLE [dbo].[Assignment]
/****** Object:  Table [dbo].[Import]    Script Date: 04/02/2020 15:02:20 ******/
DROP TABLE [dbo].[Import]
GO

/****** Object:  Table [dbo].[Import]    Script Date: 03/02/2020 16:20:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Import](
	[PK_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer] [varchar](30) NOT NULL,
	[Project] [varchar](30) NOT NULL,
	[Task] [varchar](30) NOT NULL,
	[Employee] [varchar](45) NOT NULL,
	[Date] [date] NOT NULL,
	[Duration] [decimal](10, 2) NOT NULL,
 CONSTRAINT [PK_Import] PRIMARY KEY CLUSTERED 
(
	[PK_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Import', @level2type=N'COLUMN',@level2name=N'PK_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Projet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Import', @level2type=N'COLUMN',@level2name=N'Project'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Import', @level2type=N'COLUMN',@level2name=N'Customer'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'T�che' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Import', @level2type=N'COLUMN',@level2name=N'Task'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Salari�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Import', @level2type=N'COLUMN',@level2name=N'Employee'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Import', @level2type=N'COLUMN',@level2name=N'Date'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dur�e' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Import', @level2type=N'COLUMN',@level2name=N'Duration'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire Import' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Import', @level2type=N'CONSTRAINT',@level2name=N'PK_Import'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Import (table temporaire)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Import'
GO

/****** Object:  Table [dbo].[Customer]    Script Date: 08/01/2020 09:38:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Customer](
	[PK_ID] [int] IDENTITY(1,1) NOT NULL,
	[Wording] [varchar](50) NOT NULL UNIQUE,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[PK_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'PK_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libell�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Wording'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'CONSTRAINT',@level2name=N'PK_Customer'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO


/****** Object:  Table [dbo].[Assignment]    Script Date: 08/01/2020 09:39:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Assignment](
	[PK_ID] [int] IDENTITY(1,1) NOT NULL,
	[Wording] [varchar](30) NOT NULL UNIQUE,
	[Display_order] [int] NOT NULL UNIQUE,
	[Billable] [bit] NOT NULL,
 CONSTRAINT [PK_Assignment] PRIMARY KEY CLUSTERED 
(
	[PK_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Assignment', @level2type=N'COLUMN',@level2name=N'PK_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libell�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Assignment', @level2type=N'COLUMN',@level2name=N'Wording'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ordre d''affichage' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Assignment', @level2type=N'COLUMN',@level2name=N'Display_order'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Facturable' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Assignment', @level2type=N'COLUMN',@level2name=N'Billable'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Assignment', @level2type=N'CONSTRAINT',@level2name=N'PK_Assignment'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Affectation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Assignment'
GO


/****** Object:  Table [dbo].[Employee]    Script Date: 08/01/2020 09:43:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Employee](
	[PK_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
	[First_name] [varchar](20) NOT NULL,
	[Apprentice] [bit] NOT NULL,
 CONSTRAINT Name_First_name_constraint UNIQUE (Name, First_name),
 
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[PK_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'PK_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nom' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pr�nom' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'First_name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Apprenti' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Apprentice'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'CONSTRAINT',@level2name=N'PK_Employee'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Salari�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO


/****** Object:  Table [dbo].[Project]    Script Date: 08/01/2020 09:47:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Project](
	[PK_ID] [int] IDENTITY(1,1) NOT NULL,
	[Wording] [varchar](30) NOT NULL,
	[Estimated_time] [decimal](10, 2) NOT NULL,
	[Selection_time_spent] [bit] NOT NULL,
	[Display_kpi] [bit] NOT NULL,
	[FK_ID_Customer] [int] NOT NULL,
	CONSTRAINT Wording_FK_ID_Customer_constraint UNIQUE (Wording, FK_ID_Customer),

 CONSTRAINT [PK_Project] PRIMARY KEY CLUSTERED 
(
	[PK_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Project]  WITH CHECK ADD  CONSTRAINT [FK_Project_Customer] FOREIGN KEY([FK_ID_Customer])
REFERENCES [dbo].[Customer] ([PK_ID])
GO

ALTER TABLE [dbo].[Project] CHECK CONSTRAINT [FK_Project_Customer]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'COLUMN',@level2name=N'PK_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libell�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'COLUMN',@level2name=N'Wording'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Temps �stim�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'COLUMN',@level2name=N'Estimated_time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S�lection temps pass�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'COLUMN',@level2name=N'Selection_time_spent'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Affichage kpi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'COLUMN',@level2name=N'Display_kpi'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� �trang�re Client' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'COLUMN',@level2name=N'FK_ID_Customer'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'CONSTRAINT',@level2name=N'PK_Project'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Projet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� �trang�re Projet_Client' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'CONSTRAINT',@level2name=N'FK_Project_Customer'
GO


/****** Object:  Table [dbo].[Record]    Script Date: 18/02/2020 14:54:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Record](
	[PK_ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NOT NULL,
	[Duration] [decimal](10, 2) NOT NULL,
	[Apprentice] [bit] NOT NULL,
	[Billable] [bit] NOT NULL,
	[FK_ID_Employee] [int] NOT NULL,
	[FK_ID_Assignment] [int] NOT NULL,
	[FK_ID_Project] [int] NOT NULL,
 CONSTRAINT [PK_Record] PRIMARY KEY CLUSTERED 
(
	[PK_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Record]  WITH CHECK ADD  CONSTRAINT [FK_Record_Assignment] FOREIGN KEY([FK_ID_Assignment])
REFERENCES [dbo].[Assignment] ([PK_ID])
GO

ALTER TABLE [dbo].[Record] CHECK CONSTRAINT [FK_Record_Assignment]
GO

ALTER TABLE [dbo].[Record]  WITH CHECK ADD  CONSTRAINT [FK_Record_Employee] FOREIGN KEY([FK_ID_Employee])
REFERENCES [dbo].[Employee] ([PK_ID])

GO

ALTER TABLE [dbo].[Record] CHECK CONSTRAINT [FK_Record_Employee]
GO

ALTER TABLE [dbo].[Record]  WITH CHECK ADD  CONSTRAINT [FK_Record_Project] FOREIGN KEY([FK_ID_Project])
REFERENCES [dbo].[Project] ([PK_ID])

GO

ALTER TABLE [dbo].[Record] CHECK CONSTRAINT [FK_Record_Project]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record', @level2type=N'COLUMN',@level2name=N'PK_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record', @level2type=N'COLUMN',@level2name=N'Date'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dur�e' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record', @level2type=N'COLUMN',@level2name=N'Duration'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Apprenti' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record', @level2type=N'COLUMN',@level2name=N'Apprentice'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Facturable' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record', @level2type=N'COLUMN',@level2name=N'Billable'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� �trang�re Salari�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record', @level2type=N'COLUMN',@level2name=N'FK_ID_Employee'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� �trang�re Affectation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record', @level2type=N'COLUMN',@level2name=N'FK_ID_Assignment'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� �trang�re Projet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record', @level2type=N'COLUMN',@level2name=N'FK_ID_Project'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record', @level2type=N'CONSTRAINT',@level2name=N'PK_Record'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� �trang�re Enregistrement_Affectation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record', @level2type=N'CONSTRAINT',@level2name=N'FK_Record_Assignment'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� �trang�re Enregistrement_Salari�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record', @level2type=N'CONSTRAINT',@level2name=N'FK_Record_Employee'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� �trang�re Enregistrement_Projet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Record', @level2type=N'CONSTRAINT',@level2name=N'FK_Record_Project'
GO


/****** Object:  Table [dbo].[Task]    Script Date: 08/01/2020 11:07:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Task](
	[PK_ID] [int] IDENTITY(1,1) NOT NULL,
	[Wording] [varchar](30) NOT NULL,
	[FK_ID_Project] [int] NOT NULL,
	[FK_ID_Assignment] [int] NOT NULL,
	CONSTRAINT Wording_FK_ID_Project_constraint UNIQUE (Wording, FK_ID_Project),

 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[PK_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_Assignment] FOREIGN KEY([FK_ID_Assignment])
REFERENCES [dbo].[Assignment] ([PK_ID])
GO

ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_Assignment]
GO

ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_Project] FOREIGN KEY([FK_ID_Project])
REFERENCES [dbo].[Project] ([PK_ID])
GO

ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_Project]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� primaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Task', @level2type=N'COLUMN',@level2name=N'PK_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libell�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Task', @level2type=N'COLUMN',@level2name=N'Wording'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� �trang�re Projet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Task', @level2type=N'COLUMN',@level2name=N'FK_ID_Project'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� �trang�re Affectation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Task', @level2type=N'COLUMN',@level2name=N'FK_ID_Assignment'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'T�che' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Task'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� �trang�re T�che_Affectation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Task', @level2type=N'CONSTRAINT',@level2name=N'FK_Task_Assignment'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cl� �trang�re T�che_Projet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Task', @level2type=N'CONSTRAINT',@level2name=N'FK_Task_Project'
GO

