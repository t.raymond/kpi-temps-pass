﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using ApplidevKPI.Models;

namespace ApplidevKPI.Models.Static
{
    public class ViewModelData
    {
        public static List<int> GetYears(List<Record> records) //return years in record list to a int list
        {
            List<int> Years = new List<int>();
            try
            {
                foreach (Record record in records)
                {
                    //ISO8601 https://fr.wikipedia.org/wiki/ISO_8601#Num%C3%A9ro_de_semaine
                    int daysOffset = DayOfWeek.Thursday - record.Date.DayOfWeek;
                    DateTime Thursday = record.Date.AddDays(daysOffset);
                    if (!Years.Any(y => y.Equals(Thursday.Year))) //if the year of the thursday record date not exist in Years
                    {
                        Years.Add(Thursday.Year);// add year of the thursday record date
                    }
                }
            }
            finally
            {
                if ((!(Years.Any())) || Years == null) //if Years is empty or Years is null
                {
                    Years.Add(0);//add "Aucun élément"
                }
            }

            Years = Years.OrderByDescending(x => x).ToList();

            return Years;
        }
        public static RefusedImport CsvReader(dbEntities db, System.IO.StreamReader fichier)//return bool is refused and error lists
        {
            int compteurLigne = 0, compteurColonne = 0;
            string ligne;
            //Patern to split the csv file
            string pattern = @"(?<quoted>(?<=,""|^"")(?:""""|[\w\W]*?)*(?="",|""$))|(?<normal>(?<=,(?!"")|^(?!""))[^,]*?(?=(?<!"")$|(?<!""),))|(?<eol>\r\n|\n)";
            db.Database.ExecuteSqlCommand("TRUNCATE TABLE [dbo].[Import]");
            //Declaration of the lists
            List<string> unknownProjectsTasks = new List<string>(), unknownCustomers = new List<string>(), unknownEmployees = new List<string>();
            //Import is refused?
            Boolean refused = false;


            while ((ligne = fichier.ReadLine()) != null)
            {
                if (compteurLigne > 0)
                {
                    Import import = new Import();
                    compteurColonne = 0;
                    #region "Read CSV line by line"
                    foreach (System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(ligne, pattern))
                    {

                        switch (compteurColonne)
                        {
                            case 0://Project
                                    import.Project = m.Value;
                                break;
                            case 1://Client
                                    import.Customer = m.Value;
                                break;
                            case 3://Task
                                    import.Task = m.Value;
                                break;
                            case 4://Employee + check in database (user)
                                string[] split = m.Value.Split(' ');
                                string firstName = split[0];
                                string name = split[1];
                                if ((db.Employee.Where(x => x.First_name.Equals(firstName) && x.Name.Equals(name)).Any()))
                                {
                                    import.Employee = m.Value;
                                }
                                else
                                {
                                    unknownEmployees.Add(m.Value);//add Employee in error list if not exist
                                }
                                break;
                            case 8://Date (Start Time)
                                import.Date = DateTime.Parse(m.Value, new CultureInfo("en-CA"));
                                break;
                            case 13://Duration decimal
                                import.Duration = Math.Round(decimal.Parse(m.Value.Replace('.', ',')), 2);
                                break;
                        }
                        compteurColonne++;
                    }
                    #endregion

                    //Check the customer in database
                    if (db.Customer.Where(x => x.Wording.Equals(import.Customer)).Any())
                    {
                        if (!(db.Project.Where(x => (x.Wording.Equals(import.Project)) && (x.Customer.Wording.Equals(import.Customer))).Any()))//add error in list if project of customer not exist
                        {
                            unknownProjectsTasks.Add("Projet \"" + import.Project + "\" inconnu du client " + import.Customer + ".");
                        }
                        else if (!(db.Project.Where(x => (x.Wording.Equals(import.Project)) && (x.Task.Where(t => t.Wording.Equals(import.Task)).Any())).Any())) //add error in list if task of project of customer not exist
                        {
                            unknownProjectsTasks.Add("Tâche \"" + import.Task + "\" inconnue du projet " + import.Project + " du client " + import.Customer + ".");
                        }
                    }
                    else //Add customer in list if not exist
                    {
                        unknownCustomers.Add(import.Customer);
                    }

                    if (!(unknownEmployees.Any() || unknownCustomers.Any() || unknownProjectsTasks.Any()))
                    {
                        db.Import.Add(import);//add import in database if list error is empty
                    }
                    else
                    {
                        refused = true;
                    }
                }
                compteurLigne++;
            }
            fichier.Close();
            return new RefusedImport(refused, unknownProjectsTasks, unknownCustomers, unknownEmployees);
        }
    }
}