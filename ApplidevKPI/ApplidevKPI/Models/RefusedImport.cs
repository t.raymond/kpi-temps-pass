﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplidevKPI.Models
{
    public sealed class RefusedImport
    {
        public List<string> UnknownProjectsTasks { get; set; }
        public List<string> UnknownCustomers { get; set; }
        public List<string> UnknownEmployees { get; set; }

        public Boolean Refused { get; set; }

        public RefusedImport(Boolean refused, List<string> unknownProjectsTasks, List<string> unknownCustomers, List<string> unknownEmployees)
        {
            this.Refused = refused;
            this.UnknownProjectsTasks = unknownProjectsTasks;
            this.UnknownCustomers = unknownCustomers;
            this.UnknownEmployees = unknownEmployees;
        }
    
    }
}