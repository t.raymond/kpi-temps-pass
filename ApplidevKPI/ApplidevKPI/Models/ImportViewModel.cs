﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplidevKPI.Models
{
    public class ImportViewModel
    {
        public int PK_ID { get; set; }
        public string Customer { get; set; }
        public string Project { get; set; }
        public string Task { get; set; }
        public int FK_ID_Assignment { get; set; }
        public int FK_ID_Employee { get; set; }
        public int FK_ID_Project { get; set; }
        public string Employee { get; set; }
        public bool Apprentice { get; set; }
        public DateTime Date { get; set; }
        public decimal Duration { get; set; }
    }
}