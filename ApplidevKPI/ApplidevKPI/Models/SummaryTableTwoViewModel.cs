﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplidevKPI.Models
{
    public class SummaryTableTwoViewModel
    {
        public string Customer { get; set; }
        public string Project { get; set; }
        public decimal Estimated_time { get; set; }
        public decimal Time_spent { get; set; }
        public decimal Apprentice_time { get; set; }
        public decimal Paid_time { get; set; }
        public decimal Rest { get; set; }
        public decimal Deviation { get; set; }
        public bool Display_kpi { get; set; }

        public string GetDeviationColor()
        {
            string color = "#ff0000";//red <= 0
            if(Deviation > 0 && Deviation < 5)
            {
                color = "#EE7701";//orange
            }
            else if (Deviation > 5 && Deviation < 10)
            {
                color = "#ffc000";//yellow
            }
            else if (Deviation > 10 && Deviation < 15)
            {
                color = "#c5e0b4";//lightgreen
            }
            else if (Deviation > 15)
            {
                color = "#70ad47";//darkgreen
            }
            return color;
        }

        public string GetCross()
        {
            string cross = "";
            if (Display_kpi)
            {
                cross = "x";
            }
            return cross;
        }
    }
}