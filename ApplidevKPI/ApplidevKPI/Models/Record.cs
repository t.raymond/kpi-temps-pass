//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ApplidevKPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Record
    {
        public int PK_ID { get; set; }
        public System.DateTime Date { get; set; }
        public decimal Duration { get; set; }
        public bool Apprentice { get; set; }
        public bool Billable { get; set; }
        public int FK_ID_Employee { get; set; }
        public int FK_ID_Assignment { get; set; }
        public int FK_ID_Project { get; set; }
    
        public virtual Assignment Assignment { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual Project Project { get; set; }
    }
}
