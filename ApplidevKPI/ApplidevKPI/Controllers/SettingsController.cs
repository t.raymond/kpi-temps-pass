﻿using ApplidevKPI.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ApplidevKPI.Controllers
{
    public class SettingsController : Controller
    {
        private readonly dbEntities db = new dbEntities();
        // GET: Settings
        public ActionResult Index()
        {
            ViewBag.Message = "Your setting page.";

            return View();
        }
        #region ShowList

        public ActionResult ShowAssignmentList()
        {
            List<Assignment> assignments = db.Assignment.ToList();

            return PartialView("AssignmentsList",assignments);
        }

        public ActionResult ShowProjectList()
        {
            List<Project> projects = db.Project.ToList();

            return PartialView("ProjectsList", projects);
        }
        public ActionResult ShowEmployeeList()
        {
            List<Employee> employees = db.Employee.ToList();

            return PartialView("EmployeesList", employees);
        }

        public ActionResult ShowCustomerList()
        {
            List<Customer> customers = db.Customer.ToList();

            return PartialView("CustomersList",customers);
        }

        public ActionResult ShowTaskList(int idProject)
        {
            List<Task> tasks = db.Task.Where(x => x.FK_ID_Project == idProject).ToList();

            ViewBag.ProjetName = db.Project.Find(idProject).Wording;
            ViewBag.ProjetId = idProject;

            return PartialView("TasksList", tasks);
        }
        #endregion

        #region Delete
        public ActionResult DeleteConfirmation(int id, string action, string wording)
        {
            try
            {
                string error = "";
                switch (action)
                {
                    case "DeleteAssignment":
                        if (db.Record.Where(x => x.FK_ID_Assignment == id).Any())
                        {
                            error += "Impossible de supprimer :<br/><span class='text-danger'>" + wording + "</span> est utlilisé dans la table Enregistrement.<br/>";
                        }
                        if (db.Task.Where(x => x.FK_ID_Assignment == id).Any())
                        {
                            error += "Impossible de supprimer :<br/><span class='text-danger'>" + wording + "</span/> est utlilisé dans la table Tâches pour un projet(s).";
                        }
                        break;

                    case "DeleteProject":
                        if (db.Task.Where(x => x.FK_ID_Project == id).Any())
                        {
                            error += action;
                        }
                        break;

                    case "DeleteEmployee":
                        if (db.Record.Where(x => x.FK_ID_Employee == id).Any())
                        {
                            error += "Impossible de supprimer :<br/><span class='text-danger'>" + wording + "</span/> est utlilisé dans la table Enregistrement.";
                        }
                        break;

                    case "DeleteCustomer":
                        if (db.Project.Where(x => x.FK_ID_Customer == id).Any())
                        {
                            error += "Impossible de supprimer :<br/><span class='text-danger'>" + wording + "</span/> est utlilisé dans la table Projet.";
                        }
                        break;

                    case "DeleteTask":
                        break;
                    default:
                        throw new Exception();
                }
                ViewBag.OnClick = "deleteLine(" + id + ",'" + action + "')";
                ViewBag.DeleteError = error;
                ViewBag.LineWording = wording;

                return PartialView();
            }
            catch
            {
                return Json(null);
            }
        }

        public ActionResult DeleteAssignment(int id)
        {
            try
            {
                Assignment assignment = db.Assignment.Find(id);
                db.Assignment.Remove(assignment);
                db.SaveChanges();
                return Json(true);
            }
            catch
            {
                return Json(null);
            }
        }

        public ActionResult DeleteProject(int id)
        {
            try
            {
                List<Task> tasks = db.Task.Where(x => x.FK_ID_Project == id).ToList();
                if(tasks != null)
                {
                    db.Task.RemoveRange(tasks);
                }
                Project project = db.Project.Find(id);
                db.Project.Remove(project);
                db.SaveChanges();
                return Json(true);
            }
            catch
            {
                return Json(null);
            }
        }

        public ActionResult DeleteEmployee(int id)
        {
            try
            {
                Employee employee = db.Employee.Find(id);
                db.Employee.Remove(employee);
                db.SaveChanges();
                return Json(true);
            }
            catch
            {
                return Json(null);
            }
        }

        public ActionResult DeleteCustomer(int id)
        {
            try
            {
                Customer customer = db.Customer.Find(id);
                db.Customer.Remove(customer);
                db.SaveChanges();
                return Json(true);
            }
            catch
            {
                return Json(null);
            }
        }

        public ActionResult DeleteTask(int id)
        {
            try
            {
                Task task = db.Task.Find(id);
                db.Task.Remove(task);
                db.SaveChanges();
                return Json(true);
            }
            catch
            {
                return Json(null);
            }
        }
        #endregion

        #region "Create"
        [HttpPost]
        public ActionResult FormCreate(string Pvue)
        {
            try
            {
                if (Pvue.Equals("CreateProject"))
                {
                    ViewBag.FK_ID_Customer = new SelectList(db.Customer, "PK_ID", "Wording");
                    ViewBag.ProjectDecimal = "";//decimal for imput type number empty if create action (look FormEdit action)
                }
                else if (Pvue.Equals("CreateTask"))
                {
                    ViewBag.FK_ID_Assignment = new SelectList(db.Assignment, "PK_ID", "Wording");
                }

                return PartialView(Pvue);
            }
            catch
            {
                return Json(null);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAssignment([Bind(Include = "Wording,Display_order,Billable")] Assignment assignment)
        {
            try
            {
                if (db.Assignment.Where(x => (x.Wording.Equals(assignment.Wording))).Any())
                {
                    return Json("Erreur le libellé \"" + assignment.Wording + "\" est déjà utilisé !");
                }
                else if (db.Assignment.Where(x => (x.Display_order == assignment.Display_order)).Any())
                {
                    return Json("Erreur l'ordre d'affichage " + assignment.Display_order.ToString() + " est déjà utilisé !");
                }
                if (ModelState.IsValid)
                {
                    db.Assignment.Add(assignment);
                    db.SaveChanges();
                    return Json(new { result = "success", assignment.PK_ID});
                }

                return Json("Erreur les données saisies ne sont pas conformes");
           }
            catch
            {
                return Json(null);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCustomer([Bind(Include = "Wording")] Customer customer)
        {
            try
            {
                if (db.Customer.Where(x => x.Wording.Equals(customer.Wording)).Any())
                {
                    return Json("Erreur le libellé \"" + customer.Wording + "\" est déjà utilisé !");
                }
                if (ModelState.IsValid)
                {
                    db.Customer.Add(customer);
                    db.SaveChanges();
                    return Json(new { result = "success", customer.PK_ID });
                }

                return Json("Erreur les données saisies ne sont pas conformes");
            }
            catch
            {
                return Json(null);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployee([Bind(Include = "Name,First_name,Apprentice")] Employee employee)
        {
            try
            {
                if (db.Employee.Where(x => (x.Name.Equals(employee.Name)) && (x.First_name.Equals(employee.First_name))).Any())
                {
                    return Json("Erreur salarié(e) \"" + employee.Name + " " + employee.First_name + "\" existe déjà !");
                }
                if (ModelState.IsValid)
                {
                    db.Employee.Add(employee);
                    db.SaveChanges();
                    return Json(new { result = "success", employee.PK_ID });
                }

                return Json("Erreur les données saisies ne sont pas conformes");
            }
            catch
            {
                return Json(null);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProject([Bind(Include = "Wording,Estimated_time,Selection_time_spent,Display_kpi,FK_ID_Customer")] Project project)
        {
            try
            {
                if (db.Project.Where(x => (x.Wording.Equals(project.Wording)) && (x.FK_ID_Customer == project.FK_ID_Customer)).Any())
                {
                    return Json("Erreur le projet \"" + project.Wording + "\" existe déjà pour le client \"" + db.Customer.Find(project.FK_ID_Customer).Wording + "\" !");
                }
                if (ModelState.IsValid)
                {
                    db.Project.Add(project);
                    db.SaveChanges();
                    return Json(new { result = "success", project.PK_ID });
                }

                return Json("Erreur les données saisies ne sont pas conformes");
            }
            catch
            {
                return Json(null);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTask([Bind(Include = "Wording,FK_ID_Project,FK_ID_Assignment")] Task task)
        {
            try
            {
                if (db.Task.Where(x => (x.Wording.Equals(task.Wording)) && (x.FK_ID_Project == task.FK_ID_Project)).Any())
                {
                    return Json("Erreur la tâche \"" + task.Wording + "\" existe déjà pour le projet \"" + db.Project.Find(task.FK_ID_Project).Wording + "\" !");
                }
                if (ModelState.IsValid)
                {
                    db.Task.Add(task);
                    db.SaveChanges();
                    return Json(new { result = "success", task.PK_ID });
                }

                return Json("Erreur les données saisies ne sont pas conformes");
            }
            catch
            {
                return Json(null);
            }
        }
        #endregion

        #region "Edit"
        [HttpPost]
        public ActionResult FormEdit(string Pvue, int id)
        {
            //uses the same create views
            try
            {
                switch (Pvue)
                {
                    case "CreateProject":
                        Project project = db.Project.Find(id);
                        ViewBag.FK_ID_Customer = new SelectList(db.Customer, "PK_ID", "Wording",project.FK_ID_Customer);
                        
                        ViewBag.Decimal = project.Estimated_time.ToString().Replace(",", ".");//for input type number

                        return PartialView(Pvue, project);

                    case "CreateTask":
                        Task task = db.Task.Find(id);
                        ViewBag.FK_ID_Assignment = new SelectList(db.Assignment, "PK_ID", "Wording", task.FK_ID_Assignment);

                        return PartialView(Pvue, task);

                    case "CreateAssignment":
                        Assignment assignment = db.Assignment.Find(id);
                        return PartialView(Pvue, assignment);

                    case "CreateCustomer":
                        Customer customer = db.Customer.Find(id);
                        return PartialView(Pvue, customer);

                    case "CreateEmployee":
                        Employee employee = db.Employee.Find(id);
                        return PartialView(Pvue, employee);
                    default:
                        throw new Exception();
                }
            }
            catch
            {
                return Json(null);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAssignment([Bind(Include = "PK_ID,Wording,Display_order,Billable")] Assignment assignment)
        {
            try
            {
                if (db.Assignment.Where(x => (x.Wording.Equals(assignment.Wording)) && (x.PK_ID != assignment.PK_ID)).Any())
                {
                    return Json("Erreur le libellé \"" + assignment.Wording + "\" est déjà utilisé !");
                }
                else if (db.Assignment.Where(x => (x.Display_order == assignment.Display_order) && (x.PK_ID != assignment.PK_ID)).Any())
                {
                    return Json("Erreur l'ordre d'affichage " + assignment.Display_order.ToString() + " est déjà utilisé !");
                }
                if (ModelState.IsValid)
                {
                    db.Entry(assignment).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { result = "success", assignment.PK_ID });
                }

                return Json("Erreur les données saisies ne sont pas conformes");
            }
            catch
            {
                return Json(null);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCustomer([Bind(Include = "PK_ID,Wording")] Customer customer)
        {
            try
            {

                if (db.Customer.Where(x => (x.Wording.Equals(customer.Wording)) && (x.PK_ID != customer.PK_ID)).Any())
                {
                    return Json("Erreur le libellé \"" + customer.Wording + "\" est déjà utilisé !");
                }
                if (ModelState.IsValid)
                {
                    db.Entry(customer).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { result = "success", customer.PK_ID });
                }

                return Json("Erreur les données saisies ne sont pas conformes");
            }
            catch
            {
                return Json(null);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployee([Bind(Include = "PK_ID,Name,First_name,Apprentice")] Employee employee)
        {
            try
            {
                if (db.Employee.Where(x => (x.Name.Equals(employee.Name)) && (x.First_name.Equals(employee.First_name)) && (x.PK_ID != employee.PK_ID)).Any())
                {
                    return Json("Erreur salarié(e) \"" + employee.Name +" " + employee.First_name + "\" existe déjà !");
                }
                if (ModelState.IsValid)
                {
                    db.Entry(employee).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { result = "success", employee.PK_ID });
                }

                return Json("Erreur les données saisies ne sont pas conformes");
            }
            catch
            {
                return Json(null);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProject([Bind(Include = "PK_ID,Wording,Estimated_time,Selection_time_spent,Display_kpi,FK_ID_Customer")] Project project)
        {
            try
            {
                if (db.Project.Where(x => (x.Wording.Equals(project.Wording)) && (x.FK_ID_Customer == project.FK_ID_Customer) && (x.PK_ID != project.PK_ID)).Any())
                {
                    return Json("Erreur le projet \"" + project.Wording + "\" existe déjà pour le client \""+ db.Customer.Find(project.FK_ID_Customer).Wording + "\" !");
                }
                if (ModelState.IsValid)
                {
                    db.Entry(project).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { result = "success", project.PK_ID });
                }

                return Json("Erreur les données saisies ne sont pas conformes");
            }
            catch
            {
                return Json(null);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTask([Bind(Include = "PK_ID,Wording,FK_ID_Project,FK_ID_Assignment")] Task task)
        {
            try
            {
                if (db.Task.Where(x => (x.Wording.Equals(task.Wording)) && (x.FK_ID_Project == task.FK_ID_Project) && (x.PK_ID != task.PK_ID)).Any())
                {
                    return Json("Erreur la tâche \"" + task.Wording + "\" existe déjà pour le projet \"" + db.Project.Find(task.FK_ID_Project).Wording + "\" !");
                }
                if (ModelState.IsValid)
                {
                    db.Entry(task).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { result = "success", task.PK_ID });
                }

                return Json("Erreur les données saisies ne sont pas conformes");
            }
            catch
            {
                return Json(null);
            }
        }
        #endregion
    }
}