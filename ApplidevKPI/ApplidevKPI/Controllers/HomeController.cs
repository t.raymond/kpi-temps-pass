﻿using ApplidevKPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data.Entity.Validation;
using ApplidevKPI.Models.Static;
using ClosedXML.Excel;

namespace ApplidevKPI.Controllers
{
    public class HomeController : Controller
    {
        private readonly dbEntities db = new dbEntities();//data basse
        private static List<Dictionary<string, decimal>> ExportTableOne = new List<Dictionary<string, decimal>>();
        private static List<SummaryTableTwoViewModel> ExportTableTwo = new List<SummaryTableTwoViewModel>();
        public ActionResult Index()//form for upload a csv file
        {
            return View();
        }

        
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase files)//action for read file
        {
            //Single File Upload
            try
            {

                if (System.IO.Directory.Exists(Server.MapPath("~/App_Data/uploads")))//suppression du repertoire
                {
                    System.IO.Directory.Delete(Server.MapPath("~/App_Data/uploads"), true);
                    Directory.CreateDirectory(Server.MapPath("~/App_Data/uploads"));
                }

                // Verify that the user selected a file
                if (files.ContentLength > 0 && files.ContentLength < 83886080 && (files.ContentType.Equals("application/octet-stream")|| files.ContentType.Equals("text/csv")|| files.ContentType.Equals("application/vnd.ms-excel")))
                {
                    // extract only the filename
                    string fileName = Path.GetFileName(files.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    string path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
                    files.SaveAs(path);
                    System.IO.StreamReader fichier = new System.IO.StreamReader(path);
                    RefusedImport refusedImport = ViewModelData.CsvReader(db, fichier);
                    if (refusedImport.Refused)//if CsvReader return refused true
                    {
                        ViewBag.refusedImport = refusedImport;
                        return View("RefusedImport");
                    }
                    else //if CsvReader return refused false
                    {
                        db.SaveChanges(); //add imports in import table

                        #region "Create List ImportViewModel for SelectAssignment View"
                        List<ImportViewModel> importList = new List<ImportViewModel>();
                        foreach (Import i in db.Import.ToList())
                        {
                            //Search project and assignment default
                            Project project = db.Project.Where(x => (x.Wording.Equals(i.Project)) && (x.Customer.Wording.Equals(i.Customer))).FirstOrDefault();
                            int PK_ID_AssignmentDefault = project.Task.Where(t => t.Wording.Equals(i.Task)).FirstOrDefault().Assignment.PK_ID;
                            //Search enmployee
                            string[] nameSplit = i.Employee.Split(' ');
                            string name = nameSplit[1], firstName = nameSplit[0];
                            Employee employee = db.Employee.Where(x => x.Name.Equals(name) && x.First_name.Equals(firstName)).FirstOrDefault();
                            ImportViewModel displayVM = new ImportViewModel()
                            {
                                PK_ID = i.PK_ID,
                                Customer = i.Customer,
                                Project = i.Project,
                                Task = i.Task,
                                FK_ID_Assignment = PK_ID_AssignmentDefault,
                                FK_ID_Employee = employee.PK_ID,
                                FK_ID_Project = project.PK_ID,
                                Employee = i.Employee,
                                Date = i.Date,
                                Duration = i.Duration,
                                Apprentice = employee.Apprentice,
                               
                            };
                            importList.Add(displayVM);
                        }
                        ViewBag.HtmlAlert = null;
                        ViewBag.Assignments = db.Assignment.ToList();
                        //get import date
                        DateTime importDate = importList.FirstOrDefault().Date;
                        //ISO8601 use the date of thursday of the week of import
                        int daysOffset = DayOfWeek.Thursday - importDate.DayOfWeek;
                        importDate = importDate.AddDays(daysOffset);//important for the year display of the ViewBag.DateImport
                        //get the start and end week of import
                        DayOfWeek day = importDate.DayOfWeek;
                        int days = day - DayOfWeek.Monday;
                        DateTime startWeek = importDate.AddDays(-days);
                        DateTime endWeek = startWeek.AddDays(6);
                        //ISO8601 https://fr.wikipedia.org/wiki/ISO_8601#Num%C3%A9ro_de_semaine
                        int nbWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(importDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                        ViewBag.DateImport = "Semaine "+ nbWeek.ToString() + "/"+ importDate.Year.ToString();


                        if(db.Record.Where(x => (x.Date >= startWeek) && (x.Date <= endWeek)).Any())//if record at week import exist
                        {
                            
                            ViewBag.HtmlAlert = " : Des données existent déjà celles-ci seront écrasées.";
                        }
                        #endregion

                        importList = importList.OrderBy(x => x.Customer).ThenBy(x => x.Project).ThenBy(x => x.Date).ThenBy(x => x.Employee).ToList();

                        return View("SelectAssignment",importList);
                    }
                }
                else if(files.ContentLength > 0)
                {
                    ViewBag.Message = "Format incorrect ✘";
                }


            }
            catch (DbEntityValidationException)
            {
                ViewBag.Message = "Fichier non Conforme ✘";
            }
            catch
            {
                ViewBag.Message = "Fichier non Reçu ✘";
            }

            return View();            
        }

        public ActionResult Search()//action for select a summary table in record table 
        {
            List<Record> records = db.Record.ToList();//order by date

            ViewBag.Years = ViewModelData.GetYears(records);//return int list of years

            return View();
        }

        [HttpPost]
        public ActionResult AddRecord(List<ImportViewModel> importViewModels)//action for convert csv data to record table
        {
            try
            {
                
                List<Record> records = new List<Record>();
                foreach (ImportViewModel import in importViewModels) // create records with importViewModels
                {
                    Record record = new Record()
                    {
                        Apprentice = import.Apprentice,
                        Date = import.Date,
                        Duration = import.Duration,
                        FK_ID_Assignment = import.FK_ID_Assignment,
                        Billable = db.Assignment.Find(import.FK_ID_Assignment).Billable,
                        FK_ID_Employee = import.FK_ID_Employee,
                        FK_ID_Project = import.FK_ID_Project,
                    };
                    records.Add(record);
                }

                //get the start and end week of import
                DateTime recordDate = records.FirstOrDefault().Date;
                DayOfWeek day = recordDate.DayOfWeek;
                int days = day - DayOfWeek.Monday;
                DateTime startWeek = recordDate.AddDays(-days);
                DateTime endWeek = startWeek.AddDays(6);
                if (db.Record.Where(x => (x.Date >= startWeek) && (x.Date <= endWeek)).Any())//if record at week records exist
                {
                    List<Record> records1 = db.Record.Where(x => (x.Date >= startWeek) && (x.Date <= endWeek)).ToList();
                    db.Record.RemoveRange(records1);
                    db.SaveChanges();
                }
                db.Record.AddRange(records);
                db.SaveChanges();
                int nbWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(startWeek, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                string redirectUrl = new UrlHelper(Request.RequestContext).Action("SummaryTables", "Home", new { year = startWeek.Year, week = nbWeek });
                return Json(new { year = startWeek.Year, week = nbWeek });

            }
            catch
            {
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult GetWeeks(int yearSelected)//return a view of list of the nb weeks in record table for a year 
        {
            try
            {
                //for the nbweek 53 add te previous and next year of year
                List<DateTime> dates = db.Record.Where(x => (x.Date.Year == yearSelected) || (x.Date.Year == yearSelected-1) || (x.Date.Year == yearSelected + 1)).Select(x => x.Date).ToList();
                List<int> weeks = new List<int>();
                foreach (DateTime d in dates)
                {
                    //ISO8601 https://fr.wikipedia.org/wiki/ISO_8601#Num%C3%A9ro_de_semaine
                    int nbWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(d, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                    if (!weeks.Contains(nbWeek)) //if nb week not exist in weeks
                    {
                        if (nbWeek == 53)
                        {
                            //ISO8601 check the year of thursday of the week
                            int daysOffset = DayOfWeek.Thursday - d.DayOfWeek;
                            DateTime Thursday = d.AddDays(daysOffset);
                            if(Thursday.Year == yearSelected)
                            {
                                weeks.Add(nbWeek); //if the year of thursday is yearSelected
                            }
                        }
                        else if(d.Year == yearSelected)//add the nb week if year equals at yearSelected
                        {
                            weeks.Add(nbWeek);
                        }
                    }
                }
                weeks = weeks.OrderBy(x => x).ToList();

                return PartialView(weeks);
            }
            catch
            {
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult SummaryTables(int year,int week)//create two table for a week in a year
        {
            try
            {
                #region "Convert year and week to start and end date"
                int tempWeek = week;
                DateTime jan1 = new DateTime(year, 1, 1);
                int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

                // Use first Thursday in January to get first week of the year as
                // it will never be in Week 52/53
                DateTime firstThursday = jan1.AddDays(daysOffset);
                var cal = CultureInfo.CurrentCulture.Calendar;
                //ISO8601 https://fr.wikipedia.org/wiki/ISO_8601#Num%C3%A9ro_de_semaine
                int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                // As we're adding days to a date in Week 1,
                // we need to subtract 1 in order to get the right date for week #1
                if (firstWeek == 1)
                {
                    tempWeek -= 1;
                }
                // Using the first Thursday as starting week ensures that we are starting in the right year
                // then we add number of weeks multiplied with days
                DateTime result = firstThursday.AddDays(tempWeek * 7);
                // Subtract 3 days from Thursday to get Monday, which is the first weekday in ISO8601
                DateTime startWeekDate = result.AddDays(-3);
                DateTime endWeekDate = result.AddDays(+3);
                #endregion

                //initialize Lists and var
                List<Dictionary<string, decimal>> tableOne = new List<Dictionary<string, decimal>>();
                List<SummaryTableTwoViewModel> tableTwo = new List<SummaryTableTwoViewModel>();
                List<Assignment> assignments = db.Assignment.OrderBy(x => x.Display_order).ToList();
                List<Employee> employees = db.Employee.ToList();
                List<Record> recordsWeek = db.Record.Where(x => (x.Date >= startWeekDate) && (x.Date <= endWeekDate)).ToList();
                List<Project> projects = db.Project.Where(x => x.Selection_time_spent).OrderBy(x => x.FK_ID_Customer).ToList();
                decimal hoursBillable = 0;
                decimal percent = 0;
                #region "Table one"
                //Créate Dictionnary(column by employee)
                foreach (Employee employee in employees)
                {
                    Dictionary<string, decimal> column = new Dictionary<string, decimal>
                {
                    { "presence", Decimal.Round(recordsWeek.Where(x => x.FK_ID_Employee == employee.PK_ID).Select(x => x.Duration).Sum(), 2) }
                };
                    foreach (Assignment assignment in assignments)
                    {
                        column.Add(assignment.PK_ID.ToString(), Decimal.Round(recordsWeek.Where(x => (x.FK_ID_Employee == employee.PK_ID) && (x.FK_ID_Assignment == assignment.PK_ID)).Select(x => x.Duration).Sum(), 2));
                    }
                    hoursBillable = Decimal.Round(recordsWeek.Where(x => (x.FK_ID_Employee == employee.PK_ID) && (x.Billable == true)).Select(x => x.Duration).Sum(), 2);
                    percent = 0;
                    if (column["presence"] > 0)
                    {
                        percent = decimal.Round((hoursBillable * 100) / column["presence"], 2);
                    }

                    column.Add("billablePercent", percent);
                    tableOne.Add(column);
                }
                //Column Total and %
                Dictionary<string, decimal> columnTotal = new Dictionary<string, decimal>
                {
                    { "presence", Decimal.Round(recordsWeek.Select(x => x.Duration).Sum(), 2) }
                };
                Dictionary<string, decimal> columnPercent = new Dictionary<string, decimal>
                {
                    { "presence", 100 }
                };
                foreach (Assignment assignment in assignments)
                {
                    string key = assignment.PK_ID.ToString();
                    columnTotal.Add(key, Decimal.Round(recordsWeek.Where(x => x.FK_ID_Assignment == assignment.PK_ID).Select(x => x.Duration).Sum(), 2));
                    if (columnTotal["presence"] > 0)
                    {
                        columnPercent.Add(key, Decimal.Round((columnTotal[key] / columnTotal["presence"]) * 100, 2));
                    }
                    else
                    {
                        columnPercent.Add(key, 0);
                    }
                }
                hoursBillable = Decimal.Round(recordsWeek.Where(x => (x.Billable == true)).Select(x => x.Duration).Sum(), 2);
                percent = 0;
                if (columnTotal["presence"] > 0)
                {
                    percent = decimal.Round((hoursBillable * 100) / columnTotal["presence"], 2);
                }
                columnTotal.Add("billablePercent", percent);
                columnPercent.Add("billablePercent", 0);
                tableOne.Add(columnTotal);
                tableOne.Add(columnPercent);



                ViewBag.TableOne = tableOne;
                ViewBag.Employees = employees;
                ViewBag.Assignments = assignments;
                #endregion

                #region "Table two"
                foreach (Project project in projects)
                {

                    SummaryTableTwoViewModel LineTableTwo = new SummaryTableTwoViewModel()
                    {
                        Customer = project.Customer.Wording,
                        Project = project.Wording,
                        Estimated_time = project.Estimated_time,
                        //incluannnnnnnnnnnnnt
                        Time_spent = Decimal.Round(db.Record.Where(x => (x.FK_ID_Project == project.PK_ID) && (x.Date <= endWeekDate)).Select(x => x.Duration).DefaultIfEmpty(0).Sum(), 2),
                        Apprentice_time = Decimal.Round(db.Record.Where(x => (x.FK_ID_Project == project.PK_ID) && (x.Apprentice) && (x.Date <= endWeekDate)).Select(x => x.Duration).DefaultIfEmpty(0).Sum(),2),
                        Display_kpi = project.Display_kpi,
                    };
                    LineTableTwo.Paid_time = Decimal.Round(LineTableTwo.Time_spent - (0.5M * LineTableTwo.Apprentice_time), 2);
                    List<Task> tasks = db.Assignment.Find(1).Task.ToList();
                    LineTableTwo.Rest = Decimal.Round(LineTableTwo.Estimated_time - LineTableTwo.Paid_time,2);
                    if(LineTableTwo.Estimated_time != 0)
                    {
                        LineTableTwo.Deviation = Decimal.Round((LineTableTwo.Rest / LineTableTwo.Estimated_time)*100, 2);
                    }
                    else
                    {
                        LineTableTwo.Deviation = 0.00M;
                    }
                    tableTwo.Add(LineTableTwo);
                }
                ViewBag.TableTwo = tableTwo;
                #endregion

                //initialize for export
                ViewBag.Filename = "KPI temps passé – S"+week.ToString()+"_"+year.ToString()+ ".xlsx";
                ExportTableOne = tableOne;
                ExportTableTwo = tableTwo;

                ViewBag.Date = week.ToString() + "/" + year.ToString();//title view

                return View();
            }
            catch
            {
                return View("Error");
            }
        }

        [HttpPost]
        public FileResult ExportToExcel(string fileName)//export summary tables to xlsx file
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                var tab1 = wb.Worksheets.Add("Facturé semaine");
                var tab2 =  wb.Worksheets.Add("Temps passés");
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                int sizeTableOne = ExportTableOne.Count;
                #region "tableOne"
                //header (first row)
                var row = tab1.Row(1);
                row.Cell(1).Value = "Type";
                int ncolumn = 2;
                foreach(Employee employee in db.Employee)
                {
                    row.Cell(ncolumn).Value = employee.First_name;
                    ncolumn++;
                }
                row.Cell(ncolumn).Value = "Total";
                row.Cell(ncolumn + 1).Value = "%";
                var header = row.Cells(1,ncolumn+1);
                //style
                header.Style.Fill.BackgroundColor = XLColor.FromHtml("#ed7d31");
                header.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                header.Style.Font.FontColor = XLColor.White;
                header.Style.Font.Bold = true;

                //secondRow
                row = tab1.Row(2);
                var cell = row.Cell(1);
                cell.Value = "Présence semaine";
                cell.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                ncolumn = 2;
                foreach (Dictionary<string, decimal> secondLine in ExportTableOne)
                {
                    cell = row.Cell(ncolumn);
                    if (secondLine != ExportTableOne[sizeTableOne - 1])
                    {
                        cell.Value = secondLine["presence"];
                    }
                    if (secondLine != ExportTableOne[sizeTableOne - 2])
                    {
                        cell.Style.Font.Bold = true;
                    }
                    ncolumn++;
                }
                row.Cells(1, sizeTableOne + 1).Style.Fill.BackgroundColor = XLColor.FromHtml("#fce4d6");

                //AssignmentRow
                int nrow = 3;
                foreach (Assignment assignment in db.Assignment)
                {
                    row = tab1.Row(nrow);
                    ncolumn = 1;
                    row.Cell(ncolumn).Value = assignment.Wording;
                    row.Cell(ncolumn).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                    foreach (Dictionary<string, decimal> dico in ExportTableOne)
                    {
                        ncolumn++;
                        row.Cell(ncolumn).Value = dico[assignment.PK_ID.ToString()];
                        if (dico == ExportTableOne[sizeTableOne - 1])
                        {
                            row.Cell(ncolumn).Style.NumberFormat.Format = "0.00\"%\"";//0.00%
                        }
                        if(dico != ExportTableOne[sizeTableOne - 2])
                        {
                            tab1.Cell(nrow, ncolumn).Style.Font.Bold = true; 
                        }
                    }
                    if((nrow % 2) == 0)//pair
                    {
                        row.Cells(1,sizeTableOne+1).Style.Fill.BackgroundColor = XLColor.FromHtml("#fce4d6");
                    }
                    else//impair
                    {
                        row.Cells(1, sizeTableOne + 1).Style.Fill.BackgroundColor = XLColor.FromHtml("#f8cbad");
                    }
                    nrow++;
                }
                //lastLine lastLine["billablePercent"]
                row = tab1.Row(nrow);
                cell = row.Cell(1);
                cell.Value = "% Facturé/Semaine";
                
                ncolumn = 2;
                foreach (Dictionary<string, decimal> lastLine in ExportTableOne)
                {
                    if (lastLine != ExportTableOne[sizeTableOne - 1])
                    {
                        row.Cell(ncolumn).Value = lastLine["billablePercent"];
                        row.Cell(ncolumn).Style.NumberFormat.Format = "0.00\"%\"";//0.00%
                    }
                    row.Cell(ncolumn).Style.Fill.BackgroundColor = XLColor.FromHtml("#fce4d6");
                    ncolumn++;
                }
                //style
                cell.Style.Fill.BackgroundColor = XLColor.FromHtml("#ed7d31");
                cell.Style.Font.FontColor = XLColor.White;
                cell.Style.Font.Bold = true;
                cell.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);


                //lineTotal
                row = row.RowBelow();
                cell = row.Cell(ncolumn - 3);
                cell.Value = "Total";
                cell.CellRight().Value = ExportTableOne[sizeTableOne - 2]["presence"];
                cell.CellRight().CellRight().Value = 100;
                //style
                cell.CellRight().Style.NumberFormat.NumberFormatId = 2;
                cell.CellRight().CellRight().Style.NumberFormat.Format = "0.00\"%\"";//0.00%
                row.CellsUsed().Style.Font.Bold = true;
                row.CellsUsed().Style.Fill.BackgroundColor = XLColor.FromHtml("#f8cbad");
                cell.Style.Fill.BackgroundColor = XLColor.FromHtml("#ed7d31");
                cell.Style.Font.FontColor = XLColor.White;
                cell.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                tab1.Range(nrow + 1, ncolumn - 3, nrow + 1, ncolumn - 1).Style.Border.InsideBorder = XLBorderStyleValues.Thick;//border for total line
                tab1.Range(nrow + 1, ncolumn - 3, nrow + 1, ncolumn - 1).Style.Border.InsideBorderColor = XLColor.White;
                tab1.Range(nrow + 1, ncolumn - 3, nrow + 1, ncolumn - 1).Style.Border.TopBorder = XLBorderStyleValues.Thick;
                tab1.Range(nrow + 1, ncolumn - 3, nrow + 1, ncolumn - 1).Style.Border.TopBorderColor = XLColor.White;

                //table style
                var tablestyle1 = tab1.Range(1, 2, nrow-1, ncolumn-2);
                tablestyle1.Style.NumberFormat.Format = "0.00;-;-"; //0.00 or - for 0
                tab1.ColumnsUsed().Width = 10;
                tab1.Column(1).Width = 20;
                tab1.Rows(1, nrow + 1).Height = 18;
                tab1.Range(1,1,nrow,ncolumn-1).Style.Border.InsideBorder = XLBorderStyleValues.Thick;
                tab1.Range(1, 1, nrow, ncolumn - 1).Style.Border.InsideBorderColor = XLColor.White;
                #endregion

                #region "tableTwo"
                // header(first row)
                row = tab2.Row(1);
                row.Cell(1).Value = "Client";
                row.Cell(2).Value = "Projet";
                row.Cell(3).Value = "Temps estimé (jour)";
                row.Cell(4).Value = "Temps passé à date (jour)";
                row.Cell(5).Value = "Dont Apprentis";
                row.Cell(6).Value = "Temps payé(KPI)";
                row.Cell(7).Value = "Reste";
                row.Cell(8).Value = "Déviation";
                row.Cell(9).Value = "Affichage KPI ?";

                //style
                header = row.Cells(1, 9);
                header.Style.Fill.BackgroundColor = XLColor.FromHtml("#ed7d31");

                //tbody
                nrow = 2;
                foreach(SummaryTableTwoViewModel line in ExportTableTwo)
                {
                    row = tab2.Row(nrow);
                    row.Cell(1).Value = line.Customer;
                    row.Cell(2).Value = line.Project;
                    row.Cell(3).Value = line.Estimated_time;
                    row.Cell(4).Value = line.Time_spent;
                    row.Cell(5).Value = line.Apprentice_time;
                    row.Cell(6).Value = line.Paid_time;
                    cell = row.Cell(7);
                    cell.Value = line.Rest;
                    cell.Style.Fill.BackgroundColor = XLColor.FromHtml(line.GetDeviationColor());
                    if (line.Deviation < 0)
                    {
                        row.Cell(8).Value = line.Deviation;
                        row.Cell(8).Style.NumberFormat.Format = "0.00\"%\"";//0.00%
                    }
                    row.Cell(9).Value = line.GetCross();
                    nrow++;
                }
                //table style
                tab2.Column(1).Width = 20;
                tab2.Column(2).Width = 28;
                tab2.Columns(3, 6).Width = 24;
                tab2.Columns(7, 9).Width = 12;
                tab2.Columns(6, 7).Style.Font.Bold = true;
                nrow = tab2.LastRowUsed().RowNumber();
                ncolumn = tab2.LastColumnUsed().ColumnNumber();
                tab2.Range(2, 3, nrow, ncolumn - 2).Style.NumberFormat.NumberFormatId = 2;//0.00
                var tablestyle2 = tab2.Range(1, 1, nrow, ncolumn).CreateTable();
                tablestyle2.Theme = XLTableTheme.TableStyleMedium14;//table theme

                //legend table
                tab2.Cell(2, ncolumn + 2).Value = "R <= 0%";
                tab2.Cell(2, ncolumn + 2).Style.Fill.BackgroundColor = XLColor.FromHtml("#ff0000");//red

                tab2.Cell(3, ncolumn + 2).Value = "R > 0%";
                tab2.Cell(3, ncolumn + 2).Style.Fill.BackgroundColor = XLColor.FromHtml("#EE7701");//orange

                tab2.Cell(4, ncolumn + 2).Value = "R > +5%";
                tab2.Cell(4, ncolumn + 2).Style.Fill.BackgroundColor = XLColor.FromHtml("#ffc000");//yellow

                tab2.Cell(5, ncolumn + 2).Value = "R > +10%";
                tab2.Cell(5, ncolumn + 2).Style.Fill.BackgroundColor = XLColor.FromHtml("#c5e0b4");//lightgreen

                tab2.Cell(6, ncolumn + 2).Value = "R > +15%";
                tab2.Cell(6, ncolumn + 2).Style.Fill.BackgroundColor = XLColor.FromHtml("#70ad47");//darkgreen
                //style
                tab2.Range(2, ncolumn + 2, 6, ncolumn + 2).Style.Font.Bold = true;
                #endregion

                //Generation of Excel document
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(
                        stream.ToArray(),
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        fileName);
                }
            }
        }
    }
}