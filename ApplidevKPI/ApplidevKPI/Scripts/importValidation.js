﻿function submitListImport() {//function to send a list of importViewModel to Home/AddRecord
    if (($("#import_list").length)) {//if tbody exist
        $("#sa_submit").removeAttr("onclick");
        var importViewModels = [];
        $("#import_list > tr").each(function () {
            var $itm = $(this);
            var obj = {
                Date: "",
                Duration: "",
                Apprentice: false,
                FK_ID_Employee: 0,
                FK_ID_Assignment: 0,
                FK_ID_Project: 0,
                Task: "",
                Project: "",
                Customer: "",
            };
            var compteur = 0;
            $itm.find("td").each(function () {
                compteur++;
                switch (compteur) {
                    //FK_ID_Project
                    case 2:
                        obj.FK_ID_Project = $(this).attr("value");
                    //FK_ID_Assignment
                    case 4:
                        obj.FK_ID_Assignment = $(this).find("#select_assignment").val();
                        break;
                    //FK_ID_Employee
                    case 5:
                        obj.FK_ID_Employee = $(this).attr("value");
                        break;
                    //Apprentice
                    case 6:
                            obj.Apprentice = $(this).find("input:checkbox").first().is(":checked");
                        break;
                    //Date
                    case 7:
                        obj.Date = $(this).attr("value");
                        break;
                    //Duration
                    case 8:
                        obj.Duration = $(this).attr("value");
                        break;
                }
            });
            importViewModels.push(obj);
        });
        $.ajax({//call the controller
            type: "post",
            url: '/Home/AddRecord',
            data: {
                importViewModels: importViewModels,
            },
            success: function (data) {
                //window.location.href = data;
                $.ajax({
                    type: "post",
                    url: '/Home/SummaryTables',
                    data: {
                        year: data.year,
                        week: data.week,
                    },
                    success: function (view) {
                        document.open();
                        document.write(view);
                        document.close();
                    },
                    error: function () {
                        alert('Une erreur est survenue');
                    }
                });
            },
            error: function () {
                alert('Une erreur est survenue');
            }
        });
    }
    else {
        alert("Erreur pas de valeurs");
    }
}