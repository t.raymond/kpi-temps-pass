﻿function weekOfYear() {//to give week at select Weeks
    var yearSelected = parseInt($("#years :selected").val());
    if (yearSelected !== null && !isNaN(yearSelected)) {
        $.ajax({ //send the year selected to getWeeks action of Home controleur
            type: "post",
            url: '/Home/GetWeeks',
            data: {
                yearSelected: yearSelected,
            },
            success: function (data) {
                $("#weeks").empty().append(data);//append selected html
            },
            error: function () {
                alert('Une erreur est survenue');
            }
        });
    }
}
weekOfYear();
$("#years").change(function () {//modify if year change
    weekOfYear();
});