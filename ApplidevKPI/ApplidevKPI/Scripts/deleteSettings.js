﻿//function to display confirmation modal 
function deleteConfirmation(e, action, wording) {
    var id = e.id;
    if (id !== null) {
        $.ajax({ //call controller for display modal
            type: "post",
            url: '/Settings/DeleteConfirmation',
            data: {
                id: id,
                action: action,
                wording: wording,
            },
            success: function (data) {
                $("#confirmDelete").remove();//delete modal
                $("body").append(data);//add modal
                $("#confirmDelete").modal("show");//show modal
            },
            error: function () {
                alert('Une erreur est survenue');
            }
        });
    }
    else {
        alert("erreur id");
    }
}
//function to delete item
function deleteLine(id, action) {
    $.ajax({//call the delete action in controller
        type: "post",
        url: '/Settings/' + action,
        data: {
            id: id,
        },
        success: function (data) {
            if (data == true) {
                //remove modal-backdrop
                $("body").removeClass("modal-open");
                $("#confirmDelete").nextAll("div.modal-backdrop").first().remove();
                //remove modal
                $("#confirmDelete").remove();
                
                $("a[id='" + id + "']a[onclick*='" + action + "']").parent("td").parent("tr").remove();//remove line
                //$("#confirmDelete").modal("hide");
            }
        },
        error: function () {
            alert('Une erreur est survenue');
        }
    });
}