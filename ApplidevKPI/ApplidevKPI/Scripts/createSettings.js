﻿//function to add form Create
function addCreateLine(e, model) {
    if (($("#newLineCreate").length)) {//if form create already exist
        $("#newLineCreate").toggleClass("setting-pulse");
    }
    else {
        if (model !== null) {
            $.ajax({//call the controller
                type: "post",
                url: '/Settings/FormCreate',
                data: {
                    Pvue: "Create" + model,
                },
                success: function (data) {
                    $(e).parents("table.setting-table").find("tbody").prepend(data);//add <tr> of inputs
                    $(e).parents("table.setting-table").wrapAll('<form id ="formForNewLine" action="/Settings/Create' + model + '" method="post">');//add <form>
                    Createline(e, model);
                },
                error: function () {
                    alert('Une erreur est survenue');
                }
            });
        }
        else {
            alert("action null");
        }
    }
}
//function to submit the form
function Createline(e, model) {
    $("#formForNewLine").submit(function (event) {
        var action = $("#formForNewLine").attr("action");
        if (action !== undefined && action !== null) {
            event.preventDefault();//disable the form action
            var formData = $("#formForNewLine").serializeArray();
            //console.log(formData);
            //get the float of imput type number
            if (formData.find(item => item.name === 'Estimated_time')) {
                var decimal = formData.find(item => item.name === 'Estimated_time').value.replace(".", ","); //replace the number by a type float
                formData.find(item => item.name === 'Estimated_time').value = decimal;
            }
            //take wording or name and first name for delete modale
            var wording = "";
            if (formData.find(item => item.name === 'Wording')) {
                wording = formData.find(item => item.name === 'Wording').value;
            }
            else if ((formData.find(item => item.name === 'Name')) && (formData.find(item => item.name === 'First_name'))) {
                wording = formData.find(item => item.name === 'Name').value + " ";
                wording += formData.find(item => item.name === 'First_name').value;
            }
            $.ajax({//send form data to controller
                type: "POST",
                url: action,
                data: formData,

                success: function (data) {
                    if (data.result == "success" && data.PK_ID !== null) {
                        //delete form
                        $(e).parents("table.setting-table").unwrap();
                        var newLine = $("#newLineCreate");
                        //create tasklist link for project
                        if (model == "Project") {
                            newLine.find("#Estimated_time").replaceWith(decimal);//replace imput number by decimal
                            newLine.find("#taskLink").attr("onclick", "showTaskTable(" + data.PK_ID + ")").removeAttr("id style");

                        }
                        // create edition link
                        newLine.find("button.setting-submit-form").replaceWith(
                            $('<a>', {
                                id: data.PK_ID,
                                class: "setting-edit glyphicon glyphicon-pencil",
                                onclick: "addEditLine(this, \"" + model + "\");",
                            }));

                        newLine.find("button.setting-cancel-form").replaceWith(
                            $('<a>', {
                                id: data.PK_ID,
                                class: "setting-delete glyphicon glyphicon-trash",
                                onclick: "deleteConfirmation(this, \"Delete" + model + "\",\"" + wording + "\");",
                            }));

                        //replace the input type text with their values
                        newLine.find("input.form-control").each(function () {
                            $(this).replaceWith($(this).val());
                        });
                        //replace select by selected text
                        newLine.find("select.form-control").replaceWith(function () {
                            return $(this).children("option").filter(":selected").text();
                        });
                        //disable all checkbox
                        newLine.find("input:checkbox").each(function () {
                            $(this).attr("disabled", "disabled");
                            $(this).removeAttr("name");
                            $(this).removeAttr("id");
                        });
                        //remove hidden input
                        newLine.find("input:hidden").remove();
                        //remove hidden line for action edit
                        newLine.prev('tr[style="display: none;"]').remove();
                        //remove id
                        newLine.removeAttr("id");
                    }
                    else {
                        alert(data);
                    }
                },
                error: function () {
                    alert("Erreur : fatal exception");
                }
            });
        }
    });
}
function formCancel(e) {
    $(e).parents("table.setting-table").unwrap();//remove <form>
    $(e).parents("table.setting-table").find('tr[style="display: none;"]').removeAttr("style");//show hide <tr>
    
    $("#newLineCreate").remove();//remove <tr> of inputs
    
}