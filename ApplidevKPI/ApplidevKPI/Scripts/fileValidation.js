﻿//function to verify the extension file into the imput file
function fileValidation() {
    var fileInput = document.getElementById('input_file'); //imput
    var filePath = fileInput.value; //get path of file
    var fileName = fileInput.files[0].name;//file name
    var fileSize = fileInput.files[0].size;//file size
    var zoneText = document.getElementById('import_textzone');//text zone
    var allowedExtension = /(\.csv)$/i; //set allowed .csv
    if (!allowedExtension.exec(filePath)) { //if path contain .csv
        fileInput.value = ''; //remove file
        zoneText.className = 'import-textzone'; //reset the css of text zone
        zoneText.innerHTML = 'Importer'; //reset the name file
        alert('Veuillez importer un fichier .csv');
        return false;
    }
    else if (fileSize > 10000000) {//
        alert('Veuillez choisir un fichier moins lourd (10Mo max)');//return false if size of file is superior at 10Mo
        return false;
    }
    else {
        /*file name filter*/
        var finalName = '';//name are show
        for (var i = 0; i < fileName.length; i++) {
            if (fileName[i] == '.') {
                i = 46;
            }
            if (i == 8 && fileName.length >= 40) {
                finalName += '...<br />'//line break
                i = 36;

            }
            else if (i == 8) {//if the file name are inferior at 40 char
                finalName += '...csv';//cut at the 8 number char
                break;
            }
            if (i == 46) {
                finalName += '.csv';//50 char max in finalName
                break
            }
            finalName += fileName[i];// add char of fileName in finalName
        }
        zoneText.className = 'file-textzone import-textzone'; //modify the css of text zone
        zoneText.innerHTML = finalName; //show the file name
        return true;
    }
}