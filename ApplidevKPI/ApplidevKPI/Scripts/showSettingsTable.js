﻿function showTable(e,action) {//function to display the table settings of projects,employees,assignments and customers
    if ($("a[id*='Show']a[id *= 'List']").hasClass("setting-nav-focus")) {
        $("a[id*='Show']a[id *= 'List']").removeClass("setting-nav-focus");// remove class button press if exists
    }
    $(e).addClass("setting-nav-focus");

    $.ajax({ //call Show+action+List action of Settings controleur
        type: "post",
        url: '/Settings/' + action,
        success: function (data) {
            $("#tab_content").empty().append(data);//append Table html
        },
        error: function () {
            alert('Une erreur est survenue');
        }
    });
}

function showTaskTable(id) {//function to display the table settings of task by project

    if (id !== null) {
        $.ajax({ //send the id project to ShowTaskList action of Settings controleur
            type: "post",
            url: '/Settings/ShowTaskList',
            data: {
                idProject: id,
            },
            success: function (data) {
                $("#table_task_content").remove(); //remove the modal that contains the task table
                $("body").append(data); //add the modal taht contains the task table
                $("#table_task_content").modal("show");//show the modal taht contains the task table
            },
            error: function () {
                alert('Une erreur est survenue');
            }
        });
    }
    else {
        alert("Le projet n'existe pas");
    }
}