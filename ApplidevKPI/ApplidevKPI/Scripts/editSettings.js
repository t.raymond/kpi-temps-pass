﻿//function to add form Edit
function addEditLine(e, model) {
    if (($("#newLineCreate").length)) {//if form create already exist
        $("#newLineCreate").toggleClass("setting-pulse");
    }
    else {
        var id = e.id;
        if (model !== null && id !== null) {
            $.ajax({//call the controller
                type: "post",
                url: '/Settings/FormEdit',
                data: {
                    Pvue: "Create" + model,
                    id: id,
                },
                success: function (data) {
                    $(e).parents("table.setting-table").wrapAll('<form id ="formForNewLine" action="/Settings/Edit' + model + '" method="post">');//add <form>
                    $(e).parent("td").parent("tr").hide().after(data)//replace <tr> by inputs
                    Createline(e, model);
                },
                error: function () {
                    alert('Une erreur est survenue');
                }
            });
        }
        else {
            alert("model null");
        }
    }
}